# create a class Cat that has the following attributes
    # name, age

# instantiate the Cat with 3 cats

# make the cat meow

# make the cat scratch


# find the oldest cat


# output

# The oldest cat,___ is ___ years old

class Cat:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def meow(self):
        return f'{self.name} meowed'

    def scratch(self):
        return f'{self.name} scarcthed you'


tom = Cat('Tom', 4)
garfield = Cat('Garfield', 5)
ube = Cat('Ube', 22)

def get_oldest_cat(*args):
    return max(args)


print(f"The oldest cat is {get_oldest_cat(tom.age, garfield.age, ube.age)} years old")


