# parent class
class Animal():
    def __init__(self, animal_type):
        print('Animal Type:', animal_type)

    def hunger(self):
        return 'I am hungry'

    
# sub class
class Mammal(Animal):
    def __init__(self):
        super().__init__('Mammal') #this line allows us to use functions from the parent class
    print('Mammals give birth directly')

dog = Mammal()

print(dog.hunger())