#create a supereclass(suggestions: animal, vehicle, insects)
    #minimum of 2 method
#sub class - mammals, different types of vehicles
    #minimum of 2 method
#track how many instances werre created from subclasses

#inheritance
class Vehicle():
    vehicle_id = 1 #class variable, something that stays the same
    def __init__(self, make, year):
        self.make = make
        self.year = year
        self.id = Vehicle.vehicle_id #creating id attribute in every instance of 
        Vehicle.vehicle_id += 1 
        # print('Car type:', make)
    
    def speed(self):
        return 'Zoom Zoom Zoom mf'
    
    def gas_mileage(self):
        return 'Ehhh gas is expensive'
    
    @classmethod   #decorator
    def get_total_cars(cls): #class method
        return cls.vehicle_id - 1

# car1 = Vehicle('Ford', 1999)
# print(car1.make)

class Jdm(Vehicle):
    def __init__(self, make, year, weight):
        self.weight = weight #attribute
        super().__init__(make, year) 
    # print('Jdm is the best')
    def gas_mileage(self):
        return 'Gas is cheap'



class Usdm(Vehicle):  
    def __init__(self, make, year):
        super().__init__(make, year)
    # print('USDM is ok')

    

car1 = Jdm('Forester', 1995, 200)
print(car1.gas_mileage())
# car2 = Jdm('240sx', 1994)
car3 = Usdm('Impala', 1964)
print(car3.gas_mileage())
car4 = Usdm('Ram', 2007)

def oldest_car(car1, car2, car3, car4):
    return max(args)

print(Vehicle.get_total_cars())




