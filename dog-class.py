

class Dog():
    dog_id = 1 # class attribute - this variable is shared by all instances on Dog class
    # each dog that we instantiate from this class has a dog_id
    # tracks how many times we create a new dog using the class
    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.id = Dog.dog_id
        Dog.dog_id += 1 #adds 1 to the dog_id when a new dog is created

    def bark(self):
        return f'{self.name} says woof!!'

    def __str__(self): #string method - look this up more dunder method/magic method 
        # we don't need to call this method
        return f"Dog {self.id} named {self.name} is {self.age} years old"

    @classmethod
    def get_total_dogs(cls): #cls is our class
        return cls.dog_id - 1 # we have to - 1 because we initiall set dog_id to 1
        #so if we don't - 1 we'll have 1 extra if we try to call this call the function

dog1 = Dog('Spock', 25)
dog2 = Dog('Adobo', 4)
dog3 = Dog('Ube', 2)
dog4 = Dog('Chicken', 10)

print(Dog.get_total_dogs())