# find all the duplicates

#solution using .count() and for loop
some_list = ['a', 'b', 'c', 'd', 'b', 'b', 'm', 'n', 'n']
dupe_list = []

for item in some_list:
    if some_list.count(item) >= 2:
        if item not in dupe_list:
            dupe_list.append(item)

#solution using a dictionary to count how many times a value appears in the list
print(dupe_list)

new_dict = dict.fromkeys(some_list, 0)

for item in some_list:
    new_dict[item] += 1

new_list = []
for key, value in new_dict.items():
    if value > 1:
        new_list.append(key)
        
print(new_list)