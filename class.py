# def g_rate(t, height):
#     return height / t

# tulip = {
#     'color' : ['pinkg', 'red', 'white', 'blue'],
#     'petals' : 3,
#     'sepals' : 3,
#     'environment' : 'outdoor',
#     'stem' : True,
#     'soilType' : 'potting soil',
#     'growth_rate' : g_rate,
#     'leaves' : 2
# }


# rose = {
#     'color' : ['pinkg', 'red', 'white'],
#     'petals' : 30,
#     'sepals' : 3,
#     'environment' : 'outdoor',
#     'stem' : True,
#     'soilType' : 'potting soil',
#     'growth_rate' : g_rate,
#     'leaves' : 5
# }


# sunflower = {
#     'color' : ['pinkg', 'red', 'white'],
#     'petals' : 30,
#     'sepals' : 3,
#     'environment' : 'outdoor',
#     'stem' : True,
#     'soilType' : 'potting soil',
#     'growth_rate' : g_rate,
#     'leaves' : 5
# }


# some kind of blueprint - flower

# what are all the necessary conditions to be a flower
    # color
    # petals
    # sepals
    # env
    # stem
    # soil type
    # growth rate
    # leaves

# specific vs necessary 
    # necessary 
        # important to all the things in the class
    # specific
        # things that are specific to only one or not all of the things in a class

class PlayerCharacter:
    # self refers to the class (itself)
    def __init__(self, name, age): # dunder or magic method, known as the initializer
        self.name = name
        self.age = age # attributes


    def run(self):
        print('run!')
        return 'done'

player1 = PlayerCharacter("Edna", '44') # player1 is an instance of PlayerCharacter

print(player1.run())

